package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Node implements Serializable {

	private static final long serialVersionUID = -4333548484321514308L;
	private int mCode;
	public int mIsVisited = 0;
	private String mName;
	private ArrayList<Edge> mEdges;
	
	/* Promenljive koje se koriste za A* implementaciju */
	
	private Node mParent;
	
	//Udaljenost od pocetka
	public double mGCost;
	//Udaljenost od kraja
	public double mHCost;
	//Zbir g i h costa
	public double mFCost;
	
	public int mLineFromParent;
	
	public double mLatitude;
	public double mLongitude;
	
	/* Lista koja cuva sve kodove asocirane sa ovom stanicom
	 * koristi se samo u metodi pretrage po imenu za autocomplete
	 */
	private ArrayList<Integer> mCodes;

	public Node(int code, String name) {
		this.mCode = code;
		mName = name;
		mEdges = new ArrayList<Edge>();
		mCodes = new ArrayList<>();
		mCodes.add(code);
	}

	public void addEdge(int lineNumber, Node station, int length) {
		Edge e = new Edge(lineNumber, this, station, length);
		mEdges.add(e);
	}

	public int getCode() {
		return mCode;
	}

	public void setCode(int code) {
		this.mCode = code;
	}

	public ArrayList<Edge> getEdges() {
		return mEdges;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	@Override
	public String toString() {

		String id = "Ime stanice: ".concat(mName).concat(" Broj: ").concat(String.valueOf(mCode).concat("\nVeze:\n"));

		for (Edge veza : mEdges) {
			id = id.concat(veza.toString()).concat("\n");
		}

		return id;
	}
	
	public void addCode(int broj) {
		if (!mCodes.contains(broj)){
			mCodes.add(broj);
		}
	}
	
	public ArrayList<Integer> getCodes() {
		return mCodes;
	}

	public Node getParent() {
		return mParent;
	}

	public void setParent(Node parent) {
		mParent = parent;
	}
	
	
}
