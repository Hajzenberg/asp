package model;

import java.io.Serializable;

public class Edge implements Serializable {

	private static final long serialVersionUID = 8525757145230017498L;
	private int mLineNumber;
	private Node mStartNode, mEndNode;
	private int mLength;

	public Edge(int lineNumber, Node start, Node end, int length) {
		mLineNumber = lineNumber;
		mStartNode = start;
		mEndNode = end;
		mLength = length;
	}

	public int getLineNumber() {
		return mLineNumber;
	}

	public void setLineNumber(int lineNumber) {
		mLineNumber = lineNumber;
	}

	public Node getStartNode() {
		return mStartNode;
	}

	public void setStartNode(Node start) {
		this.mStartNode = start;
	}

	public Node getEndNode() {
		return mEndNode;
	}

	public void setEndNode(Node end) {
		this.mEndNode = end;
	}

	public int getLength() {
		return mLength;
	}

	public void setLength(int length) {
		this.mLength = length;
	}

	@Override
	public String toString() {
		return "Autobuska linija: " + mLineNumber + " Pocetna - ime: " + mStartNode.getName() + " kod: " + mStartNode.getCode()
				+ " Krajnja - ime: " + mEndNode.getName() + " kod: " + mEndNode.getCode() + " duzina " + mLength;
	}
}
