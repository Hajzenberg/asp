package actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import parse.ParseFrame;
import serialization.BelgradeBusStops;

public class DeserializeCodeModelAction extends AbstractAction {

	public DeserializeCodeModelAction() {
		putValue(NAME, "Deserialize code model");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		deserialize();
	}

	public void deserialize() {

		BelgradeBusStops parsingModel = null;

		JFileChooser jfc = new JFileChooser();
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setFileFilter(new FileNameExtensionFilter("GSP - ASP project", "gsp"));

		int returnVal = jfc.showOpenDialog(ParseFrame.getInstance());

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = jfc.getSelectedFile();

			FileInputStream fis;
			try {
				fis = new FileInputStream(file);
				ObjectInputStream ois = new ObjectInputStream(fis);
				parsingModel = (BelgradeBusStops) ois.readObject();

				if (parsingModel == null) {
					System.out.println("parsing code model je null");
				}

				parsingModel.output();

				ParseFrame.getInstance().setCodeModel(parsingModel);

				if (ParseFrame.getInstance().getCodeModel() == null) {
					System.out.println("codemodel je null");
				}

				ois.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			ParseFrame.getInstance().setCodeModel(new BelgradeBusStops());
			System.out.println("Deserialization canceled, new code model object set");
		}

	}
}
