package actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import app.Frame;
import parse.ParseFrame;
import serialization.BelgradeBusStops;

public class DeserializeNameModelActionForFrameAuto extends AbstractAction {

	public DeserializeNameModelActionForFrameAuto() {
		putValue(NAME, "Deserialize code model");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		deserialize();
	}

	public void deserialize() {

		BelgradeBusStops parsingModel = null;

		File file = new File("name_model.gsp");
		
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);				
			ObjectInputStream ois = new ObjectInputStream(fis);
			parsingModel = (BelgradeBusStops) ois.readObject();
			
			if (parsingModel == null){
				System.out.println("parsing model je null");
			}
			
			parsingModel.output();
			
			Frame.getInstance().setNameModel(parsingModel);
			
			
			
			if (Frame.getInstance().getCodeModel() == null){
				System.out.println("codemodel je null");
			}
			
			ois.close();
		} catch (Exception e){
			e.printStackTrace();
		}

	}
}
