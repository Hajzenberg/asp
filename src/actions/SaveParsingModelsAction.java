package actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

public class SaveParsingModelsAction extends AbstractAction {

	@Override
	public void actionPerformed(ActionEvent e) {
		ActionManager.getInstance().getSerializeCodeModelAction().actionPerformed(null);
		ActionManager.getInstance().getSerializeNameModelAction().actionPerformed(null);
	}
	
}
