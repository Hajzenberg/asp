package actions;

public class ActionManager {

	private static ActionManager mInstance;
	
	private FindShortestPathActionByCode mFindShortestPathActionByCode;
	private FindShortestPathActionByName mFindShortestPathActionByName;
	private SetSearchByCodeAction mSearchByCodeAction;
	private SetSearchByNameAction mSearchByNameAction;
	private DeserializeCodeModelAction mDeserializeCodeModelAction;
	private DeserializeNameModelAction mDeserializeNameModelAction;
	private OpenModelsForParsingAction mOpenParsingModelsAction;
	private ParseAction mParseAction;
	private SerializeCodeModelAction mSerializeCodeModelAction;
	private SerializeNameModelAction mSerializeNameModelAction;
	private SaveParsingModelsAction mSaveParsingModelsAction;
	private DeserializeCodeModelActionForFrame mDeserializeCodeModelActionForFrame;
	private DeserializeNameModelActionForFrame mDeserializeNameModelActionForFrame;
	private OpenParsedModelsAction mOpenParsedModelsAction;
	private FindShortestPathActionByAStarEuclid mFindShortestPathActionByAStarEuclid;
	private FindShortestPathActionByAStarManhattan mFindShortestPathActionByAStarManhattan;
	private FindShortestPathActionByAStarModified mFindShortestPathActionByAStarModified;
	private SetSearchByAStarModifiedAction mSetSearchByAStarModifiedAction;
	private OutputModelsAction mOutputModelsAction;
	private DeserializeCodeModelActionForFrameAuto mDeserializeCodeModelActionForFrameAuto;
	private DeserializeNameModelActionForFrameAuto mDeserializeNameModelActionForFrameAuto;
	private SetSearchByAStarEuclidAction mSetSearchByAStarEuclidAction;
	private SetSearchByAStarManhattanAction mSetSearchByAStarManhattanAction;
	
	private ActionManager() {
		mFindShortestPathActionByCode = new FindShortestPathActionByCode();
		mFindShortestPathActionByName = new FindShortestPathActionByName();
		mSearchByCodeAction = new SetSearchByCodeAction();
		mSearchByNameAction = new SetSearchByNameAction();
		mDeserializeCodeModelAction = new DeserializeCodeModelAction();
		mDeserializeNameModelAction = new DeserializeNameModelAction();
		mOpenParsingModelsAction = new OpenModelsForParsingAction();
		mParseAction = new ParseAction();
		mSerializeCodeModelAction = new SerializeCodeModelAction();
		mSerializeNameModelAction = new SerializeNameModelAction();
		mSaveParsingModelsAction = new SaveParsingModelsAction();
		mDeserializeCodeModelActionForFrame = new DeserializeCodeModelActionForFrame();
		mDeserializeNameModelActionForFrame = new DeserializeNameModelActionForFrame();
		mOpenParsedModelsAction = new OpenParsedModelsAction();
		mFindShortestPathActionByAStarEuclid = new FindShortestPathActionByAStarEuclid();
		mFindShortestPathActionByAStarModified = new FindShortestPathActionByAStarModified();
		mSetSearchByAStarModifiedAction = new SetSearchByAStarModifiedAction();
		mOutputModelsAction = new OutputModelsAction();
		mDeserializeCodeModelActionForFrameAuto = new DeserializeCodeModelActionForFrameAuto();
		mDeserializeNameModelActionForFrameAuto = new DeserializeNameModelActionForFrameAuto();
		mFindShortestPathActionByAStarManhattan = new FindShortestPathActionByAStarManhattan();
		mSetSearchByAStarEuclidAction = new SetSearchByAStarEuclidAction();
		mSetSearchByAStarManhattanAction = new SetSearchByAStarManhattanAction();
	}
	
	public static ActionManager getInstance() {
		if (mInstance == null){
			mInstance = new ActionManager();
		}
		return mInstance;
	}

	public FindShortestPathActionByCode getFindShortestPathActionByCode() {
		return mFindShortestPathActionByCode;
	}

	public FindShortestPathActionByName getFindShortestPathActionByName() {
		return mFindShortestPathActionByName;
	}

	public SetSearchByCodeAction getSearchByCodeAction() {
		return mSearchByCodeAction;
	}

	public SetSearchByNameAction getSearchByNameAction() {
		return mSearchByNameAction;
	}

	public DeserializeCodeModelAction getDeserializeCodeModelAction() {
		return mDeserializeCodeModelAction;
	}

	public DeserializeNameModelAction getDeserializeNameModelAction() {
		return mDeserializeNameModelAction;
	}

	public OpenModelsForParsingAction getOpenParsingModelsAction() {
		return mOpenParsingModelsAction;
	}

	public ParseAction getParseAction() {
		return mParseAction;
	}

	public SerializeCodeModelAction getSerializeCodeModelAction() {
		return mSerializeCodeModelAction;
	}

	public SerializeNameModelAction getSerializeNameModelAction() {
		return mSerializeNameModelAction;
	}

	public SaveParsingModelsAction getSaveParsingModelsAction() {
		return mSaveParsingModelsAction;
	}
	
	public DeserializeCodeModelActionForFrame getDCMAF() {
		return mDeserializeCodeModelActionForFrame;
	}
	
	public DeserializeNameModelActionForFrame getDNMAF() {
		return mDeserializeNameModelActionForFrame;
	}
	
	public OpenParsedModelsAction getOpenParsedModelsAction() {
		return mOpenParsedModelsAction;
	}

	public FindShortestPathActionByAStarEuclid getFindShortestPathActionByAStarEuclid() {
		return mFindShortestPathActionByAStarEuclid;
	}

	public FindShortestPathActionByAStarModified getFindShortestPathActionByAStarModified() {
		return mFindShortestPathActionByAStarModified;
	}

	public SetSearchByAStarModifiedAction getSetSearchByAStarModifiedAction() {
		return mSetSearchByAStarModifiedAction;
	}

	public OutputModelsAction getOutputModelsAction() {
		return mOutputModelsAction;
	}

	public DeserializeCodeModelActionForFrameAuto getDeserializeCodeModelActionForFrameAuto() {
		return mDeserializeCodeModelActionForFrameAuto;
	}

	public DeserializeNameModelActionForFrameAuto getDeserializeNameModelActionForFrameAuto() {
		return mDeserializeNameModelActionForFrameAuto;
	}

	public FindShortestPathActionByAStarManhattan getFindShortestPathActionByAStarManhattan() {
		return mFindShortestPathActionByAStarManhattan;
	}

	public SetSearchByAStarEuclidAction getSetSearchByAStarEuclidAction() {
		return mSetSearchByAStarEuclidAction;
	}

	public SetSearchByAStarManhattanAction getSetSearchByAStarManhattanAction() {
		return mSetSearchByAStarManhattanAction;
	}
}
