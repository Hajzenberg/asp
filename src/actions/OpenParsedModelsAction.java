package actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

public class OpenParsedModelsAction extends AbstractAction{

	public OpenParsedModelsAction() {
		putValue(NAME, "\u0423\u0447\u0438\u0442\u0430\u0458 \u043C\u043E\u0434\u0435\u043B\u0435");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ActionManager.getInstance().getDCMAF().deserialize();
		ActionManager.getInstance().getDNMAF().deserialize();
	}

}
