package actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import app.Frame;

public class SetSearchByAStarModifiedAction extends AbstractAction {

	public SetSearchByAStarModifiedAction() {
		putValue(NAME, "\u0410* \u043F\u0440\u0435\u0442\u0440\u0430\u0433\u0430 (\u043C\u043E\u0434\u0438\u0444\u0438\u043A\u043E\u0432\u0430\u043D\u0430)");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Frame.getInstance().getSearchButton().setAction(ActionManager.getInstance().getFindShortestPathActionByAStarModified());
		
	}

}
