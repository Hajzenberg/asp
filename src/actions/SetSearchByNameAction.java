package actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import app.Frame;

public class SetSearchByNameAction extends AbstractAction{

	public SetSearchByNameAction() {
		putValue(NAME, "\u0421\u0442\u0430\u043D\u0434\u0430\u0440\u0434\u043D\u0430 \u043F\u0440\u0435\u0442\u0440\u0430\u0433\u0430 \u043F\u043E \u0438\u043C\u0435\u043D\u0443");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Frame.getInstance().getSearchButton().setAction(ActionManager.getInstance().getFindShortestPathActionByName());
	}

}
