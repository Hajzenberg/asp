package actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import app.Frame;

public class SetSearchByAStarManhattanAction extends AbstractAction {

	public SetSearchByAStarManhattanAction() {
		putValue(NAME, "\u0410* \u043F\u0440\u0435\u0442\u0440\u0430\u0433\u0430 (\u041C\u0435\u043D\u0445\u0435\u0442\u043D)");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Frame.getInstance().getSearchButton().setAction(ActionManager.getInstance().getFindShortestPathActionByAStarManhattan());
		
	}

}
