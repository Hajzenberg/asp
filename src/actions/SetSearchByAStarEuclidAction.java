package actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import app.Frame;

public class SetSearchByAStarEuclidAction extends AbstractAction {

	public SetSearchByAStarEuclidAction() {
		putValue(NAME, "\u0410* \u043F\u0440\u0435\u0442\u0440\u0430\u0433\u0430 (\u0415\u0443\u043A\u043B\u0438\u0434)");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Frame.getInstance().getSearchButton().setAction(ActionManager.getInstance().getFindShortestPathActionByAStarEuclid());
		
	}

}
