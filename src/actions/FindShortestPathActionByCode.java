package actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;

import app.Frame;
import model.Edge;
import model.Node;

public class FindShortestPathActionByCode extends AbstractAction {

	public FindShortestPathActionByCode() {
		putValue(NAME, "\u041F\u043E \u043A\u043E\u0434\u0443");
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		String linije = "";
		String presedanje = "";

		ArrayList<Node> stations = Frame.getInstance().getCodeModel().getStations();

		/* Uzimam podatke iz textfield-ova kao stringove */
		String starCodeString = Frame.getInstance().getStartTextField().getText();
		String destinationCodeString = Frame.getInstance().getDestinationTextField().getText();

		if (starCodeString == null || destinationCodeString == null || starCodeString.length() == 0
				|| destinationCodeString.length() == 0) {
			Frame.getInstance().setLinesInfo("");
			Frame.getInstance().setStationsInfo("\u0423\u043D\u0435\u0442\u0438 \u043F\u043E\u0434\u0430\u0446\u0438 \u043D\u0438\u0441\u0443 \u0432\u0430\u043B\u0438\u0434\u043D\u0438");
			return;
		}

		int pocetna = 0, krajnja = 0;

		try {
			pocetna = Integer.parseInt(starCodeString);
			krajnja = Integer.parseInt(destinationCodeString);
		} catch (Exception e) {
			Frame.getInstance().setLinesInfo("");
			Frame.getInstance().setStationsInfo("\u0423\u043D\u0435\u0442\u0438 \u043F\u043E\u0434\u0430\u0446\u0438 \u043D\u0438\u0441\u0443 \u0432\u0430\u043B\u0438\u0434\u043D\u0438");
		}

		Node start = null, end = null;

		for (Node node : stations) {
			if (node.getCode() == pocetna)
				start = node;
			if (node.getCode() == krajnja)
				end = node;
		}

		if (start == null || end == null) {
			Frame.getInstance().setLinesInfo("");
			Frame.getInstance().setStationsInfo("Nisu pronadjene stanice za zadate parametre");
			return;
		}

		// moja implementacija reda i prolaska
		ArrayList<Node> myQueue = new ArrayList<Node>();
		
		/* Sigurno ne moze biti duze od broja stanica u gradu */
		int[] tripReconstruction = new int[stations.size()];
		
		/* Postavljamo sve vrednosti na nulu */
		for (int s = 0; s < tripReconstruction.length; s++) {
			tripReconstruction[s] = 0;
		}
		
		/* Ubacujem pocetnu stanicu u red */
		myQueue.add(start);
		
		/*
		 * i je pocetak reda, j je kraj reda ubacujemo na j-to mesto, prvo
		 * ubacivanje ce biti dummy
		 * promenljiva found je int sa vrednostima 0, 1, 2 jer sam je ranije koristio
		 * za jos neke funkcionalnosti, moze da bude i boolean
		 */
		int i = 0, j = 1, found = 0;
		
		while (i < j && found == 0) {
			
			/* Proveravam sve veze za pocetni cvor Ako cvor nije krajnji
			 * (poslednja stanica) ubacim ga na kraj reda a u rekonstrukciju
			 * puta u ubacim indeks stanice u redu iz koje smo dosli
			 */
			for (Edge v : myQueue.get(i).getEdges()) {
				
				// Prva provera nam je za pocetnu stanicu, koja je na indeksu 1
				// u redu

				/*
				 * Idemo kroz sve veze te stanice i proveravamo da li je krajnja
				 * stanica te veze i nase odrediste
				 */
				Node node = v.getEndNode();
				
				// Ako nije nase odrediste i ako nije posecena
				
				if (!node.equals(end)) {
					
					if (node.mIsVisited == 0) {
						// ubacimo je u red
						myQueue.add(j, node);
						// ubacimo u rekonstrukciju puta
						tripReconstruction[j] = i;
						j++;
						node.mIsVisited = 2;
						
						/* nasli smo krajnji dodamo u rekonstrukciju i izadjemo iz
						 * petlje
						 */
					}
				} else {
					
					// Nasli smo odrediste, setujemo da je odrediste pronadjeno
					// (=1)
					found = 1;
					myQueue.add(j, node);
					tripReconstruction[j] = i;
					// brejkujem petlju, kraj
					break;
				}
			}
			
			/*
			 * setujemo da je cvor proverene i idemo na sledeci element u redu
			 */
			myQueue.get(i).mIsVisited = 1;
			i++;
		}
		
		if (found == 0) {
			//Nije pronadjena veza, ispisujem poruku
			SwingUtilities.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					Frame.getInstance().setLinesInfo("");
					Frame.getInstance().setStationsInfo("\u041D\u0435 \u043F\u043E\u0441\u0442\u043E\u0458\u0438 \u0432\u0435\u0437\u0430");
				}
			});
			
		}

		else {
			int k = 0;
			
			/* Niz koji rekonstrukciju puta poredja po pravom redosledu */
			int[] fromStart = new int[1000];
			
			// j je poslednji indeks u redu, a to je ujedno indeks naseg
			// odredista
			while (j > 0) {
				fromStart[k++] = j;
				// sad je ovo j indeks stanice koja je prethodila
				j = tripReconstruction[j];
			}
			
			fromStart[k++] = j;
			
			/* Reversujemo niz */
			for (int m = 0; m < k / 2; m++) {
				int temp = fromStart[m];
				fromStart[m] = fromStart[k - m - 1];
				fromStart[k - m - 1] = temp;
			}
			
			/* Pravim spisak stanice kojima prolazi linija */
			for (int m = 0; m < k; m++) {
				linije = linije.concat(myQueue.get(fromStart[m]).getCode() + ", ");
			}
			
			/* Pravim listu presedanja tako da se favorizuje put sa najmanjim brojem  stanica
			 * zatim se favorizuje najmanji broj presedanja
			 * i pritom se gleda da putnik provede maksimalno vremena u autobusu u koji je prvo usao
			 * */
			
			int[] stopOver = new int[1000];
			int checkCounter = 0;
			for (int m = 0; m < k - 1; m++) {

				if (checkCounter == 0) {
					for (Edge u : myQueue.get(fromStart[m]).getEdges()) {
						if (u.getEndNode().equals(myQueue.get(fromStart[m + 1]))) {
							stopOver[checkCounter++] = u.getLineNumber();
						}
					}
				} else {
					int npr = 0;
					for (int z = 0; z < checkCounter; z++) {
						int broj = stopOver[z];
						for (Edge u : myQueue.get(fromStart[m]).getEdges()) {
							if (broj == u.getLineNumber() && u.getEndNode().equals(myQueue.get(fromStart[m + 1])))
								npr = -1;
						}
					}
					if (npr == 0) {
						for (int z = 0; z < checkCounter; z++) {
							int broj = stopOver[z];

							presedanje = presedanje.concat(broj + ", ");
						}
						checkCounter = 0;
						presedanje = presedanje
								.concat(" \u0434\u043E \u0441\u0442\u0430\u043D\u0438\u0446\u0435 " + myQueue.get(fromStart[m]).getName() + " \u0438 \u0442\u0443 \u043F\u0440\u0435\u0441\u0435\u0434\u043D\u0443\u0442\u0438 \u043D\u0430 ");
					}
					for (int z = 0; z < checkCounter; z++) {
						int broj = stopOver[z];
						npr = 0;
						for (Edge u : myQueue.get(fromStart[m]).getEdges()) {
							if (broj == u.getLineNumber() && u.getEndNode().equals(myQueue.get(fromStart[m + 1])))
								npr = -1;
						}
						if (npr == 0) {
							stopOver[z] = 0;
							for (int z1 = z; z1 < checkCounter + 1; z1++) {
								stopOver[z1] = stopOver[z1 + 1];
							}
							checkCounter--;
						}
					}
					if (checkCounter == 0) {
						for (Edge u : myQueue.get(fromStart[m]).getEdges()) {
							if (u.getEndNode().equals(myQueue.get(fromStart[m + 1]))) {
								stopOver[checkCounter++] = u.getLineNumber();
							}
						}
					}
				}
			}
			for (int z = 0; z < checkCounter; z++) {
				int stationCode = stopOver[z];
				presedanje = presedanje.concat(stationCode + ", ");
			}
			checkCounter = 0;

			presedanje = presedanje.concat(" \u0441\u0432\u0435 \u0434\u043E \u043A\u0440\u0430\u0458\u045A\u0435 \u0441\u0442\u0430\u043D\u0438\u0446\u0435 " + end.getName());
		}

		Frame.getInstance().setLinesInfo(linije);
		Frame.getInstance().setStationsInfo(presedanje);

		for (Node stanica : stations) {
			stanica.mIsVisited = 0;
		}
	}
}
