package actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import app.Frame;
import parse.ParseFrame;
import serialization.BelgradeBusStops;

public class OutputModelsAction extends AbstractAction {

	public OutputModelsAction() {
		putValue(NAME, "\u041F\u0440\u0438\u043A\u0430\u0436\u0438 \u043C\u043E\u0434\u0435\u043B\u0435 \u0443 \u043A\u043E\u043D\u0437\u043E\u043B\u0438");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Frame.getInstance().getCodeModel().output();
		Frame.getInstance().getNameModel().output();
	}
}
