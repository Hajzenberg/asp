package actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

import javax.swing.AbstractAction;

import app.Frame;
import model.Edge;
import model.Node;

public class FindShortestPathActionByAStarEuclid extends AbstractAction {

	public static final double COEF = 1;

	public FindShortestPathActionByAStarEuclid() {
		putValue(NAME, "A*");
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		String linije = "";
		String presedanje = "";

		ArrayList<Node> stanice = Frame.getInstance().getNameModel().getStations();

		/* Uzimam podatke iz textfield-ova kao stringove */
		String starCodeString = Frame.getInstance().getStartTextField().getText();
		String destinationCodeString = Frame.getInstance().getDestinationTextField().getText();

		if (starCodeString == null || destinationCodeString == null || starCodeString.length() == 0
				|| destinationCodeString.length() == 0) {
			Frame.getInstance().setLinesInfo(
					"\u0423\u043D\u0435\u0442\u0438 \u043F\u043E\u0434\u0430\u0446\u0438 \u043D\u0438\u0441\u0443 \u0432\u0430\u043B\u0438\u0434\u043D\u0438");
			Frame.getInstance().setStationsInfo("");
			return;
		}

		Node start = null, end = null;

		for (Node node : stanice) {
			if (node.getName().equalsIgnoreCase(starCodeString)) {
				start = node;
			}

			if (node.getName().equalsIgnoreCase(destinationCodeString)) {
				end = node;
			}
		}

		if (start == null || end == null) {
			Frame.getInstance().setLinesInfo(
					"\u0423\u043D\u0435\u0442\u0438 \u043F\u043E\u0434\u0430\u0446\u0438 \u043D\u0438\u0441\u0443 \u0432\u0430\u043B\u0438\u0434\u043D\u0438");
			Frame.getInstance().setStationsInfo("");
			return;
		}

		/*
		 * Prodjemo kroz sve stanice i izracunamo im udaljenost od pocetka i od
		 * cilja
		 */

		for (Node node : stanice) {
			node.mGCost = calculateEuclidan(start, node);
			node.mHCost = calculateEuclidan(node, end);
		}

		Set<Node> explored = new HashSet<Node>();

		PriorityQueue<Node> queue = new PriorityQueue<Node>(20, new Comparator<Node>() {
			// Poredimo koliki je ukapan trosak, odnosno udaljenost
			public int compare(Node i, Node j) {
				if (i.mFCost > j.mFCost) {
					return 1;
				}

				else if (i.mFCost < j.mFCost) {
					return -1;
				}

				else {

					// Poredimo koji je blizi cilju, ako im je f cost isti

					if (i.mHCost > j.mHCost) {
						return 1;
					} else if (i.mHCost < j.mHCost) {
						return -1;
					} else {
						return 0;
					}
				}
			}
		});

		queue.add(start);

		boolean found = false;

		while ((!queue.isEmpty()) && (!found)) {

			// Uzimamo iz priority queue-a stanicu sa najmanjim f troskom
			Node current = queue.poll();

			explored.add(current);

			// nasli smo krajnju stanicu
			if (current.getName().equals(end.getName())) {
				found = true;
			}

			// Proveravam
			for (Edge e : current.getEdges()) {
				Node child = e.getEndNode();

				/*
				 * Trenutni trosak koji bi dodelio childu kao udaljenost od
				 * pocetka racunam kao trenutni trosak noda na kome se nalazim
				 * (g cost) + razdaljina (trosak) od tog trenutnog do childa
				 */

				double tempGCost = current.mGCost + calculateEuclidan(current, child);
				double tempFCost = tempGCost + child.mHCost;

				/*
				 * Ako je node vec posecen i ako je f cost koji bi mu dodelio
				 * veci od trenutnog costa koji mu je dodeljen preskoci
				 * iteraciju jer nema svrhe obradjivati node
				 */

				if ((explored.contains(child)) && (tempFCost >= child.mFCost)) {
					continue;
				}

				/*
				 * 
				 */

				else if ((!queue.contains(child)) || (tempFCost < child.mFCost)) {

					child.setParent(current);
					child.mLineFromParent = e.getLineNumber();
					child.mFCost = tempFCost;
					child.mGCost = tempGCost;
					// Ne zahteva
					child.mHCost = calculateEuclidan(child, end);

					if (queue.contains(child)) {
						queue.remove(child);
					}

					queue.add(child);

				}

			}

		}

		List<Node> path = new ArrayList<Node>();

		for (Node node = end; node != null; node = node.getParent()) {
			path.add(node);
		}

		Collections.reverse(path);

		for (Node node : path) {
			linije = linije.concat(node.getCode() + "").concat(", ");
		}

		/* Poseban algoritam za ovo */

		presedanje = presedanje.concat(" sve do krajnje stanice " + end.getCode());

		Frame.getInstance().setLinesInfo(linije);
		Frame.getInstance().setStationsInfo(presedanje);

		for (Node stanica : stanice) {
			stanica.mIsVisited = 0;
		}
	}

	/*
	 * Heuristika zasnovana na euklidskoj geometriji pogodna za sve slucajeve i
	 * u opstem slucaju boljom preciznoscu, ali ne bolja od Menhetn logike u
	 * gradskim prostorima
	 */

	private double calculateEuclidan(Node start, Node end) {

		double deltaX = Math.abs(start.mLatitude - end.mLatitude);
		double deltaY = Math.abs(start.mLongitude - end.mLongitude);

		return COEF * Math.sqrt(deltaX * deltaX + deltaY * deltaY);
	}
}
