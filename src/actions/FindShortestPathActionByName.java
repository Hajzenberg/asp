package actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;

import app.Frame;
import model.Edge;
import model.Node;

public class FindShortestPathActionByName extends AbstractAction {

	public FindShortestPathActionByName() {
		putValue(NAME, "\u041F\u043E \u0438\u043C\u0435\u043D\u0443");
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		String linije = "";
		String presedanje = "";

		ArrayList<Node> stations = Frame.getInstance().getNameModel().getStations();

		/* Uzimam podatke iz textfield-ova kao stringove */
		String starCodeString = Frame.getInstance().getStartTextField().getText();
		String destinationCodeString = Frame.getInstance().getDestinationTextField().getText();

		if (starCodeString == null || destinationCodeString == null || starCodeString.length() == 0
				|| destinationCodeString.length() == 0) {
			Frame.getInstance().setLinesInfo(
					"\u0423\u043D\u0435\u0442\u0438 \u043F\u043E\u0434\u0430\u0446\u0438 \u043D\u0438\u0441\u0443 \u0432\u0430\u043B\u0438\u0434\u043D\u0438");
			Frame.getInstance().setStationsInfo("");
			return;
		}

		Node start = null, end = null;

		for (Node node : stations) {
			if (node.getName().equalsIgnoreCase(starCodeString)) {
				start = node;
			}

			if (node.getName().equalsIgnoreCase(destinationCodeString)) {
				end = node;
			}
		}

		// Check da li je validno

		if (start == null || end == null) {
			Frame.getInstance().setLinesInfo(
					"\u0423\u043D\u0435\u0442\u0438 \u043F\u043E\u0434\u0430\u0446\u0438 \u043D\u0438\u0441\u0443 \u0432\u0430\u043B\u0438\u0434\u043D\u0438");
			Frame.getInstance().setStationsInfo("");
			return;
		}

		// implementacija reda i prolaska
		ArrayList<Node> myQueue = new ArrayList<Node>();
		int[] tripRecounstruction = new int[stations.size()];
		for (int s = 0; s < tripRecounstruction.length; s++) {
			tripRecounstruction[s] = 0;
		}
		myQueue.add(start);
		int i = 0, j = 1, found = 0;
		while (i < j && found == 0) {
			for (Edge v : myQueue.get(i).getEdges()) {
				Node node = v.getEndNode();
				if (!node.equals(end)) {
					if (node.mIsVisited == 0) {
						myQueue.add(j, node);
						tripRecounstruction[j] = i;
						j++;
						node.mIsVisited = 2;
					}
				} else {
					found = 1;
					myQueue.add(j, node);
					tripRecounstruction[j] = i;
					break;
				}
			}
			myQueue.get(i).mIsVisited = 1;
			i++;
		}
		if (found == 0) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					Frame.getInstance().setLinesInfo("");
					Frame.getInstance().setStationsInfo(
							"\u041D\u0435 \u043F\u043E\u0441\u0442\u043E\u0458\u0438 \u0432\u0435\u0437\u0430");

				}
			});
		} else {
			int k = 0;
			int[] fromStart = new int[1000];
			while (j > 0) {
				fromStart[k++] = j;
				j = tripRecounstruction[j];
			}
			fromStart[k++] = j;
			for (int m = 0; m < k / 2; m++) {
				int temp = fromStart[m];
				fromStart[m] = fromStart[k - m - 1];
				fromStart[k - m - 1] = temp;
			}

			System.out.println(k);
			
			for (int m = 0; m < k; m++) {

				linije = linije.concat(myQueue.get(fromStart[m]).getName() + ", ");
			}
			
			int[] stopOver = new int[1000];
			int checkCounter = 0;

			for (int m = 0; m < k - 1; m++) {

				if (checkCounter == 0) {
					for (Edge u : myQueue.get(fromStart[m]).getEdges()) {
						if (u.getEndNode().equals(myQueue.get(fromStart[m + 1]))) {
							stopOver[checkCounter++] = u.getLineNumber();
						}
					}
				} else {
					int npr = 0;
					for (int z = 0; z < checkCounter; z++) {
						int broj = stopOver[z];
						for (Edge u : myQueue.get(fromStart[m]).getEdges()) {
							if (broj == u.getLineNumber() && u.getEndNode().equals(myQueue.get(fromStart[m + 1])))
								npr = -1;
						}
					}
					if (npr == 0) {
						for (int z = 0; z < checkCounter; z++) {
							int broj = stopOver[z];

							presedanje = presedanje.concat(broj + ", ");
						}
						checkCounter = 0;
						presedanje = presedanje.concat(" \u0434\u043E \u0441\u0442\u0430\u043D\u0438\u0446\u0435 "
								+ myQueue.get(fromStart[m]).getName()
								+ " \u0438 \u0442\u0443 \u043F\u0440\u0435\u0441\u0435\u0434\u043D\u0443\u0442\u0438 \u043D\u0430 ");
					}
					for (int z = 0; z < checkCounter; z++) {
						int broj = stopOver[z];
						npr = 0;
						for (Edge u : myQueue.get(fromStart[m]).getEdges()) {
							if (broj == u.getLineNumber() && u.getEndNode().equals(myQueue.get(fromStart[m + 1])))
								npr = -1;
						}
						if (npr == 0) {
							stopOver[z] = 0;
							for (int z1 = z; z1 < checkCounter + 1; z1++) {
								stopOver[z1] = stopOver[z1 + 1];
							}
							checkCounter--;
						}
					}
					if (checkCounter == 0) {
						for (Edge u : myQueue.get(fromStart[m]).getEdges()) {
							if (u.getEndNode().equals(myQueue.get(fromStart[m + 1]))) {
								stopOver[checkCounter++] = u.getLineNumber();
							}
						}
					}
				}
			}
			for (int z = 0; z < checkCounter; z++) {
				int broj = stopOver[z];
				presedanje = presedanje.concat(broj + ", ");
			}
			checkCounter = 0;

			presedanje = presedanje
					.concat(" \u0441\u0432\u0435 \u0434\u043E \u043A\u0440\u0430\u0458\u045A\u0435 \u0441\u0442\u0430\u043D\u0438\u0446\u0435 "
							+ end.getName());

		}

		Frame.getInstance().setLinesInfo(linije);
		Frame.getInstance().setStationsInfo(presedanje);

		for (Node stanica : stations) {
			stanica.mIsVisited = 0;
		}
	}
}
