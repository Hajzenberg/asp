package actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

public class OpenModelsForParsingAction extends AbstractAction{

	public OpenModelsForParsingAction() {
		putValue(NAME, "Otvori modele za parsiranje");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ActionManager.getInstance().getDeserializeCodeModelAction().deserialize();
		ActionManager.getInstance().getDeserializeNameModelAction().deserialize();
	}

}
