package actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import parse.ParseFrame;

public class SerializeNameModelAction extends AbstractAction {

	@Override
	public void actionPerformed(ActionEvent e) {
		serialize();
	}

	public void serialize() {

		String filePath = null;

		JFileChooser jfc = new JFileChooser();
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setFileFilter(new FileNameExtensionFilter("GSP - ASP project", "gsp"));
		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int returnVal = jfc.showSaveDialog(ParseFrame.getInstance());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			filePath = jfc.getSelectedFile().getPath();

		} else if (returnVal == JFileChooser.CANCEL_OPTION) {
			System.out.println("Canceled");
			return;
		}

		try {

			File file = new File(filePath.concat("\\name_model.gsp"));

			FileOutputStream fout = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fout);

			out.writeObject(ParseFrame.getInstance().getNameModel());
			out.flush();
			out.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

}
