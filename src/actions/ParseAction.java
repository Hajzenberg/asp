package actions;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.swing.AbstractAction;

import model.Node;
import parse.ParseFrame;
import serialization.BelgradeBusStops;

public class ParseAction extends AbstractAction {

	private static final String DELIMITER = "\n\t";

	private Node mPreviousStation;
	private int mLineNumber;
	private boolean mIsLineNumberSet;
	private boolean mIsFirstStationSet;
	private BelgradeBusStops mNameModel;
	private BelgradeBusStops mCodeModel;
	private int mPreviousDistanceFromStart;

	public ParseAction() {
		putValue(NAME, "Parsiraj");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		parseByCode();
		parseByName();
	}
	
	private void parseByCode() {
		/* Reset parmeters */
		mCodeModel = ParseFrame.getInstance().getCodeModel();
		mPreviousStation = null;
		mLineNumber = 0;
		mIsLineNumberSet = false;
		mIsFirstStationSet = false;
		mPreviousDistanceFromStart = 0;

		Node currentStation = null;

		String data = ParseFrame.getInstance().getTextForParsing();

		Scanner scanner = new Scanner(data);

		while (scanner.hasNext()) {

			String line = scanner.nextLine();

			StringTokenizer st = new StringTokenizer(line, DELIMITER);

			if (!mIsLineNumberSet) {
				mLineNumber = Integer.parseInt(st.nextToken());
				mIsLineNumberSet = true;
				continue;
			}

			/*
			 * Proveravamo da li u modelu vec postoji stanica sa istim imenom
			 * ako postoji, referenciramo je kroz current station, ako ne
			 * postoji pravimo novu stanicu, referenciramo kroz current station
			 * i dodajemo je u model
			 */

			int code = Integer.parseInt(st.nextToken());
			String name = st.nextToken();
			int distanceFromPrevious = Integer.parseInt(st.nextToken().replace(".", ""));	//dummy
			int distanceFromStart = Integer.parseInt(st.nextToken().replace(".", ""));

			if ((currentStation = mCodeModel.getStationByCode(code)) == null) {


				currentStation = new Node(code, name);

				mCodeModel.addStation(currentStation);
			}

			if (!mIsFirstStationSet) {
				mIsFirstStationSet = true;
				mPreviousStation = currentStation;
				continue;
			}
			
			mPreviousStation.addEdge(mLineNumber, currentStation,
					Math.abs(distanceFromStart - mPreviousDistanceFromStart));
			
			mPreviousStation = currentStation;
			mPreviousDistanceFromStart = distanceFromStart;

		}

		scanner.close();
		
		mCodeModel.output();
	}
	
	private void parseByName() {
		/* Reset parmeters */
		mNameModel = ParseFrame.getInstance().getNameModel();
		mPreviousStation = null;
		mLineNumber = 0;
		mIsLineNumberSet = false;
		mIsFirstStationSet = false;

		Node currentStation = null;

		String data = ParseFrame.getInstance().getTextForParsing();

		Scanner scanner = new Scanner(data);

		while (scanner.hasNext()) {

			String line = scanner.nextLine();

			StringTokenizer st = new StringTokenizer(line, DELIMITER);

			if (!mIsLineNumberSet) {
				mLineNumber = Integer.parseInt(st.nextToken());
				mIsLineNumberSet = true;
				continue;
			}

			/*
			 * Proveravamo da li u modelu vec postoji stanica sa istim imenom
			 * ako postoji, referenciramo je kroz current station, ako ne
			 * postoji pravimo novu stanicu, referenciramo kroz current station
			 * i dodajemo je u model
			 */

			int code = Integer.parseInt(st.nextToken());
			String name = st.nextToken();
			int distanceFromPrevious = Integer.parseInt(st.nextToken().replace(".", ""));	//dummy
			int distanceFromStart = Integer.parseInt(st.nextToken().replace(".", ""));

			if ((currentStation = mNameModel.getStationByName(name)) == null) {
				currentStation = new Node(code, name);
				mNameModel.addStation(currentStation);
			} else {
				currentStation.addCode(code);
			}

			if (!mIsFirstStationSet) {
				mIsFirstStationSet = true;
				mPreviousStation = currentStation;
				continue;
			}

			mPreviousStation.addEdge(mLineNumber, currentStation,
					distanceFromStart - mPreviousDistanceFromStart);
			
			mPreviousStation = currentStation;
			mPreviousDistanceFromStart = distanceFromStart;
		}

		scanner.close();
		
		mNameModel.output();
	}

}
