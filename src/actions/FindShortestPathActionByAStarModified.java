package actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;

import app.Frame;
import model.Edge;
import model.Node;

public class FindShortestPathActionByAStarModified extends AbstractAction {

	public static final double COST = 1;

	public FindShortestPathActionByAStarModified() {
		putValue(NAME, "A*");
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		String linije = "";
		String presedanje = "";

		ArrayList<Node> stanice = Frame.getInstance().getNameModel().getStations();

		/* Uzimam podatke iz textfield-ova kao stringove */
		String starCodeString = Frame.getInstance().getStartTextField().getText();
		String destinationCodeString = Frame.getInstance().getDestinationTextField().getText();

		if (starCodeString == null || destinationCodeString == null || starCodeString.length() == 0
				|| destinationCodeString.length() == 0) {
			Frame.getInstance().setLinesInfo(
					"\u0423\u043D\u0435\u0442\u0438 \u043F\u043E\u0434\u0430\u0446\u0438 \u043D\u0438\u0441\u0443 \u0432\u0430\u043B\u0438\u0434\u043D\u0438");
			Frame.getInstance().setStationsInfo("");
			return;
		}

		Node start = null, end = null;

		for (Node node : stanice) {
			if (node.getName().equalsIgnoreCase(starCodeString)) {
				start = node;
			}

			if (node.getName().equalsIgnoreCase(destinationCodeString)) {
				end = node;
			}
		}

		if (start == null || end == null) {
			Frame.getInstance().setLinesInfo(
					"\u0423\u043D\u0435\u0442\u0438 \u043F\u043E\u0434\u0430\u0446\u0438 \u043D\u0438\u0441\u0443 \u0432\u0430\u043B\u0438\u0434\u043D\u0438");
			Frame.getInstance().setStationsInfo("");
			return;
		}
		
		for (Node node : stanice) {
			node.mFCost = Double.MAX_VALUE;
		}

		Set<Node> explored = new HashSet<Node>();
		
		List<Node> list = new ArrayList<>();

		/* Posto nemam g i h cost, a pritom mi se f cost povecava za konstantni
		 * COST, priority queue ne moze da funkcionise planirano, pa je zamenjen
		 * obicnom arraylistom, koja uzima prvi dostupni element i zatim ga izbacuje
		 */
		
//		PriorityQueue<Node> queue = new PriorityQueue<Node>(20, new Comparator<Node>() {
//
//			public int compare(Node i, Node j) {
//				if (i.mFCost > j.mFCost) {
//					return 1;
//				}
//
//				else if (i.mFCost < j.mFCost) {
//					return -1;
//				}
//
//				else {
//					return 0;
//				}
//			}
//		});

		list.add(start);

		boolean found = false;

		while ((!list.isEmpty()) && (!found)) {

			// Uzimamo iz queue-a prvu dostupnu stanicu
			Node current = list.get(0);
			list.remove(0);

			explored.add(current);

			// nasli smo krajnju stanicu
			if (current.getName().equals(end.getName())) {
				found = true;
				break;
			}

			// Proveravam sve susede
			for (Edge e : current.getEdges()) {
				Node child = e.getEndNode();

				double tempFCost = child.mFCost + COST;

				/*
				 * Ako je node vec posecen i ako je f cost koji bi mu dodelio
				 * veci od trenutnog costa koji mu je dodeljen, preskoci
				 * iteraciju jer nema svrhe obradjivati node
				 */

				if (explored.contains(child) &&  (tempFCost >= child.mFCost)) {
					continue;
				}

				/* U suprotnom, setujem mu roditelja i pamtim vezu kojom dolazimo od roditelja
				 * da bih mogao da rekonstruisem put, setujem mu temp f trosak koji sam izracunao gore
				 * Osiguravam da se priority queue osvezi poslednjim podacima za nod
				 */

				else if ((!list.contains(child)) || (tempFCost < child.mFCost)) {

					child.setParent(current);
					child.mLineFromParent = e.getLineNumber();
					child.mFCost = tempFCost;

					if (list.contains(child)) {
						list.remove(child);
					}

					list.add(child);
				}
			}
		}

		List<Node> path = new ArrayList<Node>();

		for (Node node = end; node != null; node = node.getParent()) {
			path.add(node);
		}
		
		System.out.println(path.size());

		Collections.reverse(path);

		for (Node node : path) {
			linije = linije.concat(node.getName() + "").concat(", ");
		}

		Node previousNode = null;
		
		for (int i = 0; i < path.size(); i++) {

			if (i == 0) {
				previousNode = path.get(i);
				continue;
			}

			if (i == path.size() - 1) {
				presedanje = presedanje
						.concat("\u0421\u0442\u0438\u0433\u043B\u0438 \u0441\u0442\u0435 \u043D\u0430 \u043E\u0434\u0440\u0435\u0434\u0438\u0448\u0442\u0435 "
								+ path.get(i).getName() + "\n");
				break;
			}

			if (path.get(i).mLineFromParent != previousNode.mLineFromParent) {
				presedanje = presedanje.concat("\u0423\u0452\u0438\u0442\u0435 \u0443 " + path.get(i).mLineFromParent
						+ " \u043D\u0430 \u0441\u0442\u0430\u043D\u0438\u0446\u0438 " + previousNode.getName() + "\n");
			}
			previousNode = path.get(i);
		}

		Frame.getInstance().setLinesInfo(linije);
		Frame.getInstance().setStationsInfo(presedanje);

		for (Node stanica : stanice) {
			stanica.mIsVisited = 0;
			stanica.mFCost = Double.MAX_VALUE;
			stanica.setParent(null);
			stanica.mLineFromParent = 0;
		}
	}
}
