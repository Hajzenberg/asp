package serialization;

import java.io.Serializable;
import java.util.ArrayList;

import model.Node;

public class BelgradeBusStops implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7372794548332103544L;
	private ArrayList<Node> mBusStops;

	public BelgradeBusStops() {
		mBusStops = new ArrayList<>();
	}

	public void addStation(Node stop) {
		mBusStops.add(stop);
	}

	public Node getStationByName(String name) {

		for (int i = 0; i < mBusStops.size(); i++) {
			if (mBusStops.get(i).getName().equals(name)) {
				return mBusStops.get(i);
			}
		}
		return null;
	}

	public Node getStationByCode(int number) {
		for (int i = 0; i < mBusStops.size(); i++) {
			if (mBusStops.get(i).getCode() == number) {
				return mBusStops.get(i);
			}
		}
		return null;
	}

	public void output() {

		System.out.println("\n************************OUTPUT*************************\n");

		for (Node stanica : mBusStops) {
			System.out.println(stanica.toString());
		}

	}

	public ArrayList<Node> getStations() {
		return mBusStops;
	}

}
