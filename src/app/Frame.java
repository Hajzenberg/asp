package app;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import actions.ActionManager;
import parse.ParseFrame;
import serialization.BelgradeBusStops;

public class Frame extends JFrame {

	private BelgradeBusStops mNameModel;
	private BelgradeBusStops mCodeModel;

	private JButton mSearchButton;

	private JTextField mStartTextField;
	private JTextField mDestinationField;

	private JTextPane mStationsInfoText;
	private JTextPane mLinesInfoText;

	private static Frame mInstance;

	private Color mBackground;

	public static Frame getInstance() {
		if (mInstance == null) {
			mInstance = new Frame();
		}
		return mInstance;
	}

	private Frame() {

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(400, 570);
		setLocationRelativeTo(null);
		setTitle("\u0413\u0421\u041F \u0414\u0415\u041C\u041E");
		initUI();

		setVisible(true);

	}

	private void initUI() {

		mBackground = new Color(31, 63, 109);

		setLayout(new BorderLayout());

		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		contentPanel.setBackground(mBackground);

		initCommandPane(contentPanel);
		initInfoPanels(contentPanel);

		add(contentPanel, BorderLayout.CENTER);

		initMenu();
		initModels();
	}

	private void initCommandPane(JPanel rootPanel) {

		Dimension fieldDimension = new Dimension(60, 22);

		JPanel commandPanel = new JPanel();
		commandPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 5));
		commandPanel.setBackground(mBackground);

		JLabel startLabel = new JLabel("\u041F\u043E\u0447\u0435\u0442\u043D\u0430:");
		startLabel.setForeground(Color.WHITE);
		JLabel destinationLabel = new JLabel("\u041A\u0440\u0430\u0458\u045A\u0430:");
		destinationLabel.setForeground(Color.WHITE);

		mStartTextField = new JTextField();
		mStartTextField.setPreferredSize(fieldDimension);
		mDestinationField = new JTextField();
		mDestinationField.setPreferredSize(fieldDimension);

		JPanel fieldPanel = new JPanel();
		fieldPanel.setBackground(mBackground);
		fieldPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 0));
		fieldPanel.add(startLabel);
		fieldPanel.add(mStartTextField);
		fieldPanel.add(destinationLabel);
		fieldPanel.add(mDestinationField);

		mSearchButton = new JButton();
		mSearchButton.setAction(ActionManager.getInstance().getFindShortestPathActionByName());
		mSearchButton.setPreferredSize(new Dimension(90, 22));
		mSearchButton.setOpaque(false);
		fieldPanel.add(mSearchButton);

		commandPanel.add(fieldPanel);
		rootPanel.add(commandPanel);
	}

	private void initInfoPanels(JPanel rootPanel) {

		Dimension areaDimension = new Dimension(350, 200);
		Dimension labelDimension = new Dimension(350, 22);

		JLabel stationsLabel = new JLabel("\u041F\u0440\u0435\u0441\u0435\u0434\u0430\u045A\u0430: ");
		stationsLabel.setForeground(Color.WHITE);
		stationsLabel.setPreferredSize(labelDimension);
		JLabel linesLabel = new JLabel("\u0421\u0442\u0430\u043D\u0438\u0446\u0435: ");
		linesLabel.setPreferredSize(labelDimension);
		linesLabel.setForeground(Color.WHITE);

		mStationsInfoText = new JTextPane();
		mStationsInfoText.setPreferredSize(areaDimension);
		mStationsInfoText.setEditable(false);
		JScrollPane sp1 = new JScrollPane(mStationsInfoText);
		mLinesInfoText = new JTextPane();
		mLinesInfoText.setPreferredSize(areaDimension);
		mLinesInfoText.setEditable(false);
		JScrollPane sp2 = new JScrollPane(mLinesInfoText);

		rootPanel.add(stationsLabel);
		rootPanel.add(sp1);
		rootPanel.add(linesLabel);
		rootPanel.add(sp2);
	}

	private void initMenu() {

		JMenuBar menuBar = new JMenuBar();
		JMenu modMenu = new JMenu("\u041D\u0430\u0447\u0438\u043D \u0440\u0430\u0434\u0430");
		menuBar.add(modMenu);
		JMenuItem nameMenuItem = new JMenuItem(ActionManager.getInstance().getSearchByNameAction());
		JMenuItem codeMenuItem = new JMenuItem(ActionManager.getInstance().getSearchByCodeAction());
		JMenuItem aStarMenuItem = new JMenuItem(ActionManager.getInstance().getSetSearchByAStarModifiedAction());
		JMenuItem aStarMenuItemM = new JMenuItem(ActionManager.getInstance().getSetSearchByAStarManhattanAction());
		aStarMenuItemM.setEnabled(false);
		JMenuItem aStarMenuItemE = new JMenuItem(ActionManager.getInstance().getSetSearchByAStarEuclidAction());
		aStarMenuItemE.setEnabled(false);
		JMenuItem openModelItem = new JMenuItem(ActionManager.getInstance().getOpenParsedModelsAction());
		openModelItem.setEnabled(false);
		JMenuItem outputMenuItem = new JMenuItem(ActionManager.getInstance().getOutputModelsAction());
		modMenu.add(nameMenuItem);
		modMenu.add(codeMenuItem);
		modMenu.add(aStarMenuItem);
		modMenu.add(aStarMenuItemM);
		modMenu.add(aStarMenuItemE);
		modMenu.addSeparator();
		modMenu.add(openModelItem);
		modMenu.add(outputMenuItem);

		add(menuBar, BorderLayout.NORTH);
	}

	private void initModels() {
		
		BelgradeBusStops parsingCodeModel = null;

		File codeFile = new File("code_model.gsp");

		FileInputStream fis;
		try {
			fis = new FileInputStream(codeFile);
			ObjectInputStream ois = new ObjectInputStream(fis);
			parsingCodeModel = (BelgradeBusStops) ois.readObject();

			if (parsingCodeModel == null) {
				System.out.println("parsing model je null");
			}

			parsingCodeModel.output();

			mCodeModel = parsingCodeModel;

			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		BelgradeBusStops parsingNameModel = null;

		File file = new File("name_model.gsp");

		try {
			fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			parsingNameModel = (BelgradeBusStops) ois.readObject();

			if (parsingNameModel == null) {
				System.out.println("parsing model je null");
			}

			parsingNameModel.output();

			mNameModel = parsingNameModel;

			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public JButton getSearchButton() {
		return mSearchButton;
	}

	public JTextField getStartTextField() {
		return mStartTextField;
	}

	public JTextField getDestinationTextField() {
		return mDestinationField;
	}

	public JTextPane getStationsInfoText() {
		return mStationsInfoText;
	}

	public JTextPane getLinesInfoText() {
		return mLinesInfoText;
	}

	public BelgradeBusStops getNameModel() {
		return mNameModel;
	}

	public void setNameModel(BelgradeBusStops mNameModel) {
		this.mNameModel = mNameModel;
	}

	public BelgradeBusStops getCodeModel() {
		return mCodeModel;
	}

	public void setCodeModel(BelgradeBusStops mCodeModel) {
		this.mCodeModel = mCodeModel;
	}

	public void setStationsInfo(String info) {
		mStationsInfoText.setText(info);
	}

	public void setLinesInfo(String info) {
		mLinesInfoText.setText(info);
	}
}
