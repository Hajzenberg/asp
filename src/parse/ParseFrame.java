package parse;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.peer.FramePeer;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;

import actions.ActionManager;
import serialization.BelgradeBusStops;

public class ParseFrame extends JFrame implements WindowListener {
	
	private BelgradeBusStops mNameModel;
	private BelgradeBusStops mCodeModel;
	
	private JTextArea mTextArea;
	
	private static ParseFrame mInstance;
	
	public static ParseFrame getInstance() {
		if (mInstance == null){
			mInstance = new ParseFrame();
		}
		return mInstance;
	}
	
	private ParseFrame () {
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(800, 600);
		setLocationRelativeTo(null);
		
		initUI();
		
		addWindowListener(this);
		
		setVisible(true);
	}
	
	private void initUI() {
		
		setLayout(new BorderLayout());
		
		mTextArea = new JTextArea();
		add(mTextArea, BorderLayout.CENTER);
		
		JButton button = new JButton();
		button.setAction(ActionManager.getInstance().getParseAction());
		add(button, BorderLayout.SOUTH);
		
		JMenuBar menuBar = new JMenuBar();
		JMenu file = new JMenu("Datoteke");
		menuBar.add(file);
		JMenuItem open = new JMenuItem(ActionManager.getInstance().getOpenParsingModelsAction());
		file.add(open);
		add(menuBar, BorderLayout.NORTH);
	}

	public BelgradeBusStops getNameModel() {
		return mNameModel;
	}

	public void setNameModel(BelgradeBusStops mNameModel) {
		this.mNameModel = mNameModel;
	}

	public BelgradeBusStops getCodeModel() {
		return mCodeModel;
	}

	public void setCodeModel(BelgradeBusStops mCodeModel) {
		this.mCodeModel = mCodeModel;
	}
	
	public String getTextForParsing() {
		return mTextArea.getText().toString();
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		ActionManager.getInstance().getSaveParsingModelsAction().actionPerformed(null);
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
